# IRC
[![Pipeline Status](https://gitlab.com/chiswicked/irc/badges/master/pipeline.svg)](https://gitlab.com/chiswicked/irc/pipelines)
[![Coverage Report](https://gitlab.com/chiswicked/irc/badges/master/coverage.svg)](https://gitlab.com/chiswicked/irc/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/chiswicked/irc)](https://goreportcard.com/report/gitlab.com/chiswicked/irc)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)
