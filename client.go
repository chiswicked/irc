// Copyright 2019 Norbert Metz
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

// IRC client. See RFC 1459 and RFC 2810 through 2813.

package irc

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"net"
	"net/textproto"
	"os"
	"strings"
	"sync"
	"time"
)

// ErrNotConnected error is returned by the Client's Listen method
// when the client is initialized with a nil Connection
var ErrNotConnected = errors.New("irc: not connected")

// ErrAuthFailed is returned by the Client's Listen method
// after connecting to a password protected server
// with invalid credentials.
var ErrAuthFailed = errors.New("irc: authentication failed")

// ErrHandlerNotFound is returned by the Client's Listen method
// if a message is received but the Client doesn't have a Handler set
// to handle the incoming messages.
var ErrHandlerNotFound = errors.New("irc: handler not found")

// ErrConnReadFailed error
var ErrConnReadFailed = errors.New("irc: connection read failed")

// ErrConnWriteFailed error
var ErrConnWriteFailed = errors.New("irc: connection read failed")

// ErrConnClosed is returned by the Client's Listen method
// after a call to Shutdown or Close.
var ErrConnClosed = errors.New("irc: client closed")

// Options can be used to a create a customized connection.
type Options struct {
	// Password
	Password string
	// Realname
	Realname string
	// RequestCapabilities
	RequestCapabilities []string
}

// GetDefaultOptions returns default configuration options for the client.
func GetDefaultOptions() Options {
	return Options{
		Password:            "",
		RequestCapabilities: []string{},
	}
}

// Option is a function on the options for a connection.
type Option func(*Options)

// Password is an Option to connect to the IRC server with the password provided.
func Password(password string) Option {
	return func(o *Options) {
		o.Password = password
	}
}

// Realname is an Option to connect to the IRC server with the password provided.
func Realname(realname string) Option {
	return func(o *Options) {
		o.Realname = realname
	}
}

// RequestCapabilities is an Option to request IRC server capabilites to be enabled
// upon successful connection.
func RequestCapabilities(caps ...string) Option {
	return func(o *Options) {
		o.RequestCapabilities = caps
	}
}

// Client struct
type Client struct {
	// I/O to the IRC server connection.
	conn net.Conn

	// Connection options
	opts Options

	// The time at which the client established the connection to the server.
	startTime time.Time

	// The name the client will use in the chat that it's attempting to join.
	currentNick string
	desiredNick string

	// Channels
	writeChan chan string
	errChan   chan error

	// Message handling
	handler Handler
}

// NewClient returns a new Client given a conn, which is used by
// the Client to read IRC Messages from and write IRC Messages to.
func NewClient(nickName string, options ...Option) *Client {
	c := &Client{
		currentNick: nickName,
		desiredNick: nickName,
		opts:        GetDefaultOptions(),
		writeChan:   make(chan string),
	}

	for _, opt := range options {
		opt(&c.opts)
	}

	if c.opts.Realname == "" {
		c.opts.Realname = nickName
	}

	return c
}

// Disconnect func
func (c *Client) Disconnect(msg ...string) time.Duration {
	quitMsg := "Goodbye for now..."
	if len(msg) > 0 {
		quitMsg = strings.Join(msg, " ")
	}
	c.Quit(quitMsg)
	return time.Since(c.startTime)
}

// ListenAndHandle listens on the given connenction for IRC messages
// and handles them as defined by the provided Handler.
// If no Handler is provided, a default system handler (NewSysHandleMux)
// is used for essential operations such as maintaining the connection
// and basic error handling.
func (c *Client) ListenAndHandle(conn net.Conn, handler Handler) error {
	var wg sync.WaitGroup

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	if conn == nil {
		return ErrNotConnected
	}
	c.conn = conn

	errChan := make(chan error, 1)
	quitChan := make(chan error, 1)

	if handler == nil {
		c.handler = NewSysHandleMux()
	} else {
		c.handler = handler
	}

	go func() {
		wg.Add(1)
		defer wg.Done()
		defer cancel()
		c.handleErr(ctx, errChan, quitChan)
	}()

	go func() {
		wg.Add(1)
		defer wg.Done()
		defer cancel()
		c.handleRead(ctx, errChan)
	}()

	go func() {
		wg.Add(1)
		defer wg.Done()
		defer cancel()
		c.handleWrite(ctx, errChan)
	}()

	// Register user on IRC server
	if len(c.opts.Password) > 0 {
		c.Pass(c.opts.Password)
	}
	if len(c.opts.RequestCapabilities) > 0 {
		c.CapReq(c.opts.RequestCapabilities...)
	}
	c.Nick(c.desiredNick)
	c.User(c.desiredNick, "0.0.0.0", "0.0.0.0", c.opts.Realname)
	c.startTime = time.Now()

	// Wait for an error from any goroutine or for the context to time out, then
	// signal we're exiting and wait for the goroutines to exit.
	wg.Wait()
	select {
	case err := <-quitChan:
		return err
	default:
		return nil
	}
}

func (c *Client) handleErr(ctx context.Context, errch chan error, quitch chan error) {
	for {
		select {
		case <-ctx.Done():
			return
		case err := <-errch:
			fmt.Fprintln(os.Stderr, "ERROR: ", err.Error())
			switch err {

			// Minor issues, ignore
			case ErrEmptyMessage, ErrInvalidMessage:

			// Critical error, quit
			default:
				quitch <- err
				return
			}
		default:
		}
	}
}

func (c *Client) handleRead(ctx context.Context, errch chan error) {

	br := bufio.NewReaderSize(c.conn, 1024)
	reader := textproto.NewReader(br)

	for {
		select {
		case <-ctx.Done():
			return
		default:
		}

		// c.conn.SetReadDeadline(time.Now().Add(500 * time.Millisecond))
		line, err := reader.ReadLine()

		if err != nil {
			sendError(ErrConnClosed, errch)
			return
		}

		if c.handler == nil {
			sendError(ErrHandlerNotFound, errch)
			return
		}

		msg, err := ParseMessage(line)
		if err != nil {
			sendError(err, errch)
			break
		}

		// TODO: should this be in a go routine?
		if err := c.handler.HandleIRCMessage(c, msg); err != nil {
			sendError(err, errch)
			return
		}
	}
}

// Writes to both the IRC server and the Client's output
func (c *Client) handleWrite(ctx context.Context, errch chan error) {
	for {
		select {
		case <-ctx.Done():
			return
		case msg := <-c.writeChan:
			fmt.Fprint(os.Stdout, msg)
			_, err := c.conn.Write([]byte(msg))
			if err != nil {
				sendError(ErrConnWriteFailed, errch)
				// sendError(fmt.Errorf("%q: %w", err, ErrConnWriteFailed), errch)
				return
			}
		}
	}
}

// sendError func
func sendError(err error, ch chan error) {
	select {
	case ch <- err:
	default:
	}
}

// Close func
func (c *Client) Close() error {
	if c.conn != nil {
		return c.conn.Close()
	}
	return nil
}
