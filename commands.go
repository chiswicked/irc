// Copyright 2019 Norbert Metz
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package irc

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
)

// ErrInvalidChannelName is returned by Join
// if the IRC channel name(s) given
// do not conform to the rfc1459 specification.
var ErrInvalidChannelName = errors.New("irc: invalid channel name")

// Join is used by client to start listening a specific channel.
//
// https://tools.ietf.org/html/rfc1459#section-4.2.1
func (c *Client) Join(channel string) error {
	// TODO multi channel join with keys
	if !isValidChannel(channel) {
		return ErrInvalidChannelName
	}
	c.Sendln("JOIN %s", channel)
	return nil
}

func isValidChannel(name string) bool {
	return regexp.MustCompile(`^[#&][^\x07\x2C\s]{0,199}$`).MatchString(name)
}

// CapReq is used to request a change in capabilities associated with
// the active connection. The last parameter is a space-separated list of capabilities.
// Each capability identifier may be prefixed with a dash (-)
// to designate that the capability should be disabled.
//
// https://ircv3.net/specs/core/capability-negotiation#the-cap-req-subcommand
func (c *Client) CapReq(cap ...string) {
	caps := strings.Join(cap, " ")
	c.Sendln("CAP REQ :%s", caps)
}

// Kick can be used to forcibly remove a user from a
// channel. It 'kicks them out' of the channel (forced `Part`).
// Only a channel operator may kick another user out of a channel.
//
// https://tools.ietf.org/html/rfc1459#section-4.2.8
func (c *Client) Kick(channel string, user string, reason string) {
	// TODO multi channel multi user kick
	c.Sendln("KICK %s %s :%s", channel, user, reason)
}

// Mode is a dual-purpose command. It allows both usernames
// and channels to have their mode changed. The rationale for
// this choice is that one day nicknames will be obsolete and the
// equivalent property will be the channel.
//
// https://tools.ietf.org/html/rfc1459#section-4.2.3
func (c *Client) Mode(target string, flags string) {
	// TODO channel mode support
	// TODO mode helpers
	c.Sendln("MODE %s %s", target, flags)
}

// Nick command is used to give user a nickname or change the previous one.
//
// https://tools.ietf.org/html/rfc1459#section-4.1.2
func (c *Client) Nick(nick string) {
	c.Sendln("NICK %s", nick)
}

// Part causes the client sending the message to be removed
// from the list of active users for all given channels listed in the
// parameter string.
//
// https://tools.ietf.org/html/rfc1459#section-4.2.2
func (c *Client) Part(channel string) {
	c.Sendln("PART %s", channel)
}

// Pass command is used to set a 'connection password'. The password
// can and must be set before any attempt to register the connection is made.
//
// https://tools.ietf.org/html/rfc1459#section-4.1.1
func (c *Client) Pass(password string) {
	c.Sendln("PASS %s", password)
}

// Pong message is a reply to ping message.
//
// https://tools.ietf.org/html/rfc1459#section-4.6.3
func (c *Client) Pong(daemon string) {
	// TODO is : necessary?
	c.Sendln("PONG :%s", daemon)
}

// PrivMsg is used to send private messages between users. `receiver`
// is the nickname of the receiver of the message. `receiver` can also
// be a list of names or channels
//
// https://tools.ietf.org/html/rfc1459#section-4.4.1
func (c *Client) PrivMsg(msg string, receiver ...string) {
	receivers := strings.Join(receiver, ",")
	c.Sendln("PRIVMSG %s :%s", receivers, msg)
}

// Quit command is used to end the client session.
//
// https://tools.ietf.org/html/rfc1459#section-4.1.6
func (c *Client) Quit(msg string) {
	c.Sendln("QUIT :%s", msg)
}

// User command is used at the beginning of connection to specify
// the username, hostname, servername and realname of a new user.
//
// https://tools.ietf.org/html/rfc1459#section-4.1.3
func (c *Client) User(username, hostname, servername, realname string) {
	c.Sendln("USER %s %s %s :%s", username, hostname, servername, realname)
}

// Invite is used to invite users to a channel. To invite a user
// to a channel which is invite only (MODE +i),
// the client sending the invite must be recognised as being a
// channel operator on the given channel.
//
// https://tools.ietf.org/html/rfc1459#section-4.2.7
func (c *Client) Invite(nickname, channel string) {
	c.Sendln("INVITE %s %s", nickname, channel)
}

// Topic is used to change or view the topic of a channel.
//
// https://tools.ietf.org/html/rfc1459#section-4.2.4
func (c *Client) Topic(channel string, topic ...string) {
	if len(topic) == 0 {
		// View channel topic
		c.Sendln("TOPIC %s", channel)
	} else {
		// Set channel topic
		c.Sendln("TOPIC %s :%s", channel, strings.Join(topic, " "))
	}
}

// send func
func (c *Client) send(msg string) {
	c.writeChan <- msg
}

// Sendln func
func (c *Client) Sendln(format string, args ...interface{}) {
	c.send(fmt.Sprintf(format+"\n\r", args...))
}
