package irc

import (
	"testing"
)

func TestIsValidChannel(t *testing.T) {
	var cases = []struct {
		in  string
		out bool
	}{
		// Has to start with # or &
		// Can't contain \x07 \x2C or space
		// Max 200 characters in total

		// Invalid
		{
			"chiswicked",
			false,
		},
		{
			"chis\x07wicked",
			false,
		},
		{
			"chis\x2Cwicked",
			false,
		},
		{
			"chis wicked",
			false,
		},
		{
			"#chis\x07wicked",
			false,
		},
		{
			"#chis\x2Cwicked",
			false,
		},
		{
			"#chis wicked",
			false,
		},
		{
			"&chis\x07wicked",
			false,
		},
		{
			"&chis\x2Cwicked",
			false,
		},
		{
			"&chis wicked",
			false,
		},
		{
			"#chiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswicked",
			false,
		},
		// Valid
		{
			"#chiswicked",
			true,
		},
		{
			"&chi$wicked",
			true,
		},
		{
			"#ch1swick3d",
			true,
		},
		{
			"&ch1$wick3d",
			true,
		},
		{
			"#chiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswickedchiswicke",
			true,
		},
	}

	for _, c := range cases {
		t.Run(c.in, func(t *testing.T) {
			b := isValidChannel(c.in)
			if b != c.out {
				t.Errorf("got %v, want %v", b, c.out)
			}
		})
	}
}
