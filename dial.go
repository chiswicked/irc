// Copyright 2019 Norbert Metz
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package irc

import (
	"crypto/tls"
	"net"
	"time"
)

// DialTimeout func
func DialTimeout(address string, timeout time.Duration) (net.Conn, error) {
	return net.DialTimeout("tcp", address, timeout)
}

// DialTLSTimeout func
func DialTLSTimeout(address string, insecure bool, timeout time.Duration) (net.Conn, error) {
	dialer := &net.Dialer{Timeout: timeout}
	tlsConfig := &tls.Config{
		InsecureSkipVerify: insecure,
	}
	return tls.DialWithDialer(dialer, "tcp", address, tlsConfig)
}
