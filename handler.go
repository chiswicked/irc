// Copyright 2019 Norbert Metz
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package irc

import (
	"errors"
	"fmt"
	"os"
)

const sysHandlerPrefix = "SYS_"

// ErrUnregisteredHandler is returned by RemoveHandler and
// RemoveDirectiveHandler if there is no handler registered for the
// given command or directive with the given name, respectively.
var ErrUnregisteredHandler = errors.New("irc: unregistered handler")

// A Handler responds to an IRC message.
//
// HandleIRCMessage is called when a client receives a message.
// If HandleIRCMessage panics the client doesn't recover the panic.
type Handler interface {
	HandleIRCMessage(c *Client, m *Message) error
}

// The HandlerFunc type is an adapter to allow the use of
// ordinary functions as IRC Message handlers. If f is a function
// with the appropriate signature, HandlerFunc(f) is a
// Handler that calls f.
type HandlerFunc func(*Client, *Message) error

// HandleIRCMessage calls f(c, m).
func (f HandlerFunc) HandleIRCMessage(c *Client, m *Message) error {
	return f(c, m)
}

func handleMsg(c *Client, msg *Message) {
	// TODO make io.Writer configurable
	fmt.Fprintln(os.Stdout, msg.String())
}

// HandleMux is an IRC message handler multiplexer.
// It matches the contents of each incoming message against
// a list of registered patterns and calls the handler
// for the pattern that matches the message, if any.
//
// Commands are IRC commands as defined in RFC 1459 and RFC 2810 through 2813.
//	mux := irc.NewHandleMux()
//	// Matches PING :irc.foonet.com
//	mux.HandleFunc("PING", "print-ping", func(c *irc.Client, msg *irc.Message) {
//		// Prints the raw PING message to standard output
//		fmt.Println(msg.String())
//	})
type HandleMux struct {
	cmd handlerRegistry
}

type handlerRegistry map[string]map[string]Handler

// NewHandleMux allocates and returns a new HandleMux.
func NewHandleMux() *HandleMux { return new(HandleMux) }

// NewSysHandleMux allocates and returns a new HandleMux
// with essential message handlers.
func NewSysHandleMux() *HandleMux {
	mux := &HandleMux{
		cmd: make(handlerRegistry),
	}

	// Register essential message handlers
	mux.HandleFunc("001", "SYS-001", handle001)
	mux.HandleFunc("PING", "SYS-ping-pong", handlePing)
	mux.HandleFunc("ERROR", "SYS-error-quit", handleError)
	mux.HandleFunc("NOTICE", "SYS-notice-quit", handleNotice)

	return mux
}

func (mux *HandleMux) handle(reg handlerRegistry, id, name string, handler Handler) error {
	if _, ok := reg[id]; !ok {
		reg[id] = make(map[string]Handler)
	}
	reg[id][name] = handler
	return nil
}

// Handle registers the handler for the given command
// with the given name. Multiple handlers can be registered for
// the same command as long as they are registered with a unique name.
// Registering a handler with a name with which a handler
// has already been registered for the same command, the original
// handler will be removed and the new one will take its place.
func (mux *HandleMux) Handle(command, name string, handler Handler) error {
	if mux.cmd == nil {
		mux.cmd = make(handlerRegistry)
	}
	return mux.handle(mux.cmd, command, name, handler)
}

// HandleFunc registers the handler function
// for the given command with the given name.
func (mux *HandleMux) HandleFunc(command, name string, handler HandlerFunc) error {
	return mux.Handle(command, name, handler)
}

// RemoveHandler deregisters the handler
// for the given command with the given name.
// Returns ErrUnregisteredHandler if there is no handler
// registered for the given command with the given name.
func (mux *HandleMux) RemoveHandler(command, name string) error {
	if _, ok := mux.cmd[command]; !ok {
		return ErrUnregisteredHandler
	}
	if _, ok := mux.cmd[command][name]; !ok {
		return ErrUnregisteredHandler
	}
	delete(mux.cmd[command], name)
	return nil
}

// HandleIRCMessage dispatches the request to all handlers that is registered
// either registered to handle the Command of the IRC Message
// or pattern most closely matches the request URL.
func (mux *HandleMux) HandleIRCMessage(c *Client, msg *Message) error {
	// Handle all messages
	handleMsg(c, msg.Copy())

	// Handle Commands
	if m, ok := mux.cmd[msg.Command]; ok {
		for _, f := range m {
			if err := f.HandleIRCMessage(c, msg.Copy()); err != nil {
				return err
			}
		}
	}
	return nil
}
