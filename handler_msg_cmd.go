// Copyright 2019 Norbert Metz
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package irc

import (
	"strings"
)

func handleError(c *Client, msg *Message) error {
	if msg != nil && msg.Command == "ERROR" {
		if strings.HasPrefix(msg.Trailing, "Closing Link:") {
			return ErrConnClosed
		}
	}
	return nil
}

func handleNotice(c *Client, msg *Message) error {
	if msg != nil && msg.Command == "NOTICE" {
		if strings.HasPrefix(msg.Trailing, "Login authentication failed") {
			return ErrAuthFailed
		}
	}
	return nil
}

// Responds to PING message with a PONG message to maintain the connection
func handlePing(c *Client, msg *Message) error {
	if msg != nil && msg.Command == "PING" {
		c.Pong(msg.Trailing)
	}
	return nil
}
