// Copyright 2019 Norbert Metz
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package irc

import "time"

// Handle RPL_WELCOME
// "Welcome to the Internet Relay Network <nick>!<user>@<host>"
func handle001(c *Client, m *Message) error {
	c.currentNick = m.Params[0]
	c.startTime = time.Now()
	return nil
}

// Handle RPL_YOURHOST
// "Your host is <servername>, running version <ver>"
func handle002(c *Client, msg *Message) error {
	return nil
}

// Handle RPL_CREATED
// "This server was created <date>"
func handle003(c *Client, msg *Message) error {
	return nil
}

// Handle RPL_MYINFO
// "<servername> <version> <available user modes> <available channel modes>"
func handle004(c *Client, msg *Message) error {
	return nil
}

// Handle RPL_BOUNCE
// "Try server <server name>, port <port number>"
func handle005(c *Client, msg *Message) error {
	return nil
}
