// Copyright 2019 Norbert Metz
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

// IRC client. See RFC 1459 and RFC 2810 through 2813.

package irc
