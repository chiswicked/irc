// Copyright 2019 Norbert Metz
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package irc

import (
	"errors"
	"strings"
)

// Message represents a structured format of an IRC message as per
// RFC 1459 specification. IRCv3.2 Message Tags are also supported.
//	['@' <tags> <SPACE>] [':' <prefix> <SPACE> ] <command> <params> <crlf>
//
// See also
//
// https://tools.ietf.org/html/rfc1459#section-2.3.1 and
// https://ircv3.net/specs/core/message-tags-3.2.html
type Message struct {
	// Tags represent optional IRCv3.2 <tags>.
	*Tags
	// Prefix represents the optional <prefix>.
	*Prefix
	// Command represents the IRC <command> being called.
	Command string
	// Params represents the list of <middle> <params>.
	Params []string
	// Trailing represents the last ':' prefixed <trailing> <params>.
	Trailing string

	raw string
}

// ErrEmptyMessage is returned by ParseMessage if the IRC message string
// passed in for parsing is an empty string. The IRC message is considered empty
// if it doesn't contain anything other than trailing <crlf> and/or whitespace.
var ErrEmptyMessage = errors.New("irc: empty message")

// ErrInvalidMessage is returned by ParseMessage if the message string
// passed in for parsing is invalid. A message is considered invalid
// if it doesn't conform to the RFC 1459 specification.
var ErrInvalidMessage = errors.New("irc: invalid message")

// ParseMessage parses the raw IRC message and returns the result
// as a Message struct. If the raw IRC message is empty or invalid,
// ParseMessage returns an ErrEmptyMessage or ErrInvalidMessage respectively.
// For an IRC message to be considered valid, it has to conform to the
// RFC1459 specification. Optional IRCv3.2 Message Tags are also supported.
func ParseMessage(msg string) (*Message, error) {
	var err error

	// If message is empty return error
	msg = strings.TrimRight(msg, "\r\n")
	msg = strings.TrimLeft(msg, " ")
	if len(msg) == 0 {
		return nil, ErrEmptyMessage
	}

	// Start parsing
	res := &Message{raw: msg}

	// Extract optional <tags>
	if msg[0] == '@' {
		split := strings.SplitN(msg, " ", 2)
		if len(split) < 2 {
			return nil, ErrInvalidMessage
		}

		res.Tags, err = parseTags(split[0][1:])
		if err != nil {
			return nil, ErrInvalidMessage
		}
		msg = split[1]
	}

	// Extract optional <prefix>
	if msg[0] == ':' {
		split := strings.SplitN(msg, " ", 2)
		if len(split) < 2 {
			return nil, ErrInvalidMessage
		}

		res.Prefix, err = parsePrefix(split[0][1:])
		if err != nil {
			return nil, ErrInvalidMessage
		}
		msg = split[1]
	}

	// Extract <command>
	split := strings.SplitN(msg, " ", 2)
	res.Command = strings.ToUpper(split[0])

	// If no <params> return with Message
	if len(split) == 1 {
		return res, nil
	}

	// If only parameter is <trailing>, extract it and return with Message
	msg = strings.TrimLeft(split[1], " ")
	if msg[0] == ':' {
		res.Trailing = msg[1:]
		return res, nil
	}

	// Extract <middle> parameters
	split = strings.SplitN(msg, " :", 2)
	res.Params = strings.Split(split[0], " ")

	// If no <trailing> return with Message
	if len(split) == 1 {
		return res, nil
	}

	// Extract <trailing> parameter
	res.Trailing = split[1]

	return res, nil
}

// String returns the IRC message in its raw format
func (msg *Message) String() string {
	return msg.raw
}

// Directive converts the `Message`'s `Trailing` parameter into a directive
// based on the prefix provided. The directive consists of
// a command and its parameters, returned as whitespace trimmed strings and
// the ok flag true, meaning success. In case the `Message` isn't a directive,
// two empty strings are returned along with the ok flag set to false.
func (msg *Message) Directive(prefix rune) (cmd string, params string, ok bool) {
	trl := strings.TrimLeft(msg.Trailing, " ")

	// Not a directive, return failure
	if msg.Command != "PRIVMSG" ||
		trl[0] != byte(prefix) {
		return "", "", false
	}

	// It is a directive, return success
	drc := strings.SplitN(trl, " ", 2)
	cmd = drc[0][1:]
	if len(drc) > 1 {
		params = strings.TrimSpace(drc[1])
	}

	return cmd, params, true
}

// Copy returns a deep copy of the Message
func (msg *Message) Copy() *Message {
	res := &Message{
		Tags:     nil,                             // pointer need to deep copy
		Prefix:   nil,                             // pointer need to deep copy
		Command:  msg.Command,                     // string safe to assign
		Params:   make([]string, len(msg.Params)), // slice need to deep copy
		Trailing: msg.Trailing,                    // string safe to assign
		raw:      msg.raw,                         // string safe to assign
	}

	if msg.Tags != nil {
		res.Tags = msg.Tags.copy()
	}

	if msg.Prefix != nil {
		res.Prefix = msg.Prefix.copy()
	}

	copy(res.Params, msg.Params)

	return res
}

// Sender returns the source of the message.
// If it's a server generated message it returns the servername (e.g irc.foonet.com).
// If it's a user generated message it returns the nick (e.g johndoe).
// Some messages don't specify the sender in which case an empty string is returned.
func (msg *Message) Sender() string {
	if msg.Prefix == nil {
		return ""
	}
	return msg.Prefix.sender()
}

// Receiver returns the target of the message. Generally messages are
// targeted at either channels (e.g #golang) or users (e.g johndoe).
// Some messages don't specify a receiver in which case an empty string is returned.
func (msg *Message) Receiver() string {
	if len(msg.Params) == 0 {
		return ""
	}
	return msg.Params[0]
}
