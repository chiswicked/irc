// Copyright 2019 Norbert Metz
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package irc

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

func TestMessageParse(t *testing.T) {
	var cases = []struct {
		in  string
		out *Message
		err error
	}{
		// Empty message tests
		{
			"",
			nil,
			ErrEmptyMessage,
		},
		{
			" ",
			nil,
			ErrEmptyMessage,
		},
		{
			"\r\n",
			nil,
			ErrEmptyMessage,
		},
		{
			" \r\n",
			nil,
			ErrEmptyMessage,
		},
		// Invalid message (tags) tests
		{
			"@ :johndoe!johndoe@irc.atw-inter.net PRIVMSG #chiswicked :hello\r\n",
			nil,
			ErrInvalidMessage,
		},
		// Invalid message (prefix) tests
		{
			"@aaa=bbb;ccc : PRIVMSG #chiswicked :hello\r\n",
			nil,
			ErrInvalidMessage,
		},
		{
			"@aaa=bbb;ccc :johndoe@johndoe!irc.atw-inter.net PRIVMSG #chiswicked :hello\r\n",
			nil,
			ErrInvalidMessage,
		},
		// Invalid message tests
		{
			"@aaa=bbb;ccc\r\n",
			nil,
			ErrInvalidMessage,
		},
		{
			"@aaa=bbb;ccc :johndoe!johndoe@irc.atw-inter.net\r\n",
			nil,
			ErrInvalidMessage,
		},
		// {
		// 	"@aaa=bbb;ccc;exam ple.com/ddd=eee :johndoe!johndoe@irc.atw-inter.net PRIVMSG #chiswicked :hello\r\n",
		// 	nil,
		// 	ErrInvalidMessage,
		// },
		// Valid message tests
		{
			"EOF",
			&Message{
				Command: "EOF",
			},
			nil,
		},
		{
			"EOF\r\n",
			&Message{
				Command: "EOF",
			},
			nil,
		},
		{
			// is this a valid case?
			"PING irc.atw-inter.net",
			&Message{
				Command: "PING",
				Params:  []string{"irc.atw-inter.net"},
			},
			nil,
		},
		{
			"PING :irc.atw-inter.net",
			&Message{
				Command:  "PING",
				Trailing: "irc.atw-inter.net",
			},
			nil,
		},
		{
			"PING :irc.atw-inter.net\r\n",
			&Message{
				Command:  "PING",
				Trailing: "irc.atw-inter.net",
			},
			nil,
		},
		{
			":irc.atw-inter.net 001 chiswicked :Welcome to the Internet Relay Network chiswicked",
			&Message{
				Prefix: &Prefix{
					Server: "irc.atw-inter.net",
				},
				Command:  "001",
				Params:   []string{"chiswicked"},
				Trailing: "Welcome to the Internet Relay Network chiswicked",
			},
			nil,
		},
		{
			"@aaa=bbb;ccc;example.com/ddd=eee :johndoe!johndoe@irc.atw-inter.net PRIVMSG #chiswicked :hello\r\n",
			&Message{
				Tags: &Tags{
					"aaa":             "bbb",
					"ccc":             "",
					"example.com/ddd": "eee",
				},
				Prefix: &Prefix{
					Nick: "johndoe",
					User: "johndoe",
					Host: "irc.atw-inter.net",
				},
				Command:  "PRIVMSG",
				Params:   []string{"#chiswicked"},
				Trailing: "hello",
			},
			nil,
		},
		{
			":irc.atw-inter.net 005 chiswicked PENALTY FNC EXCEPTS=e INVEX=I CASEMAPPING=ascii NETWORK=IRCnet :are supported by this server",
			&Message{
				Tags: nil,
				Prefix: &Prefix{
					Server: "irc.atw-inter.net",
				},
				Command: "005",
				Params: []string{
					"chiswicked",
					"PENALTY",
					"FNC",
					"EXCEPTS=e",
					"INVEX=I",
					"CASEMAPPING=ascii",
					"NETWORK=IRCnet",
				},
				Trailing: "are supported by this server",
			},
			nil,
		},
		{
			"@badge-info=;badges=;color=#FF0000;display-name=SecretRaider;emotes=;flags=;id=8f49ac68-7f67-9aa5-4723-4c02ab504d77;login=secretraider;mod=0;msg-id=raid;msg-param-displayName=SecretRaider;msg-param-login=secretraider;msg-param-profileImageURL=https://static-cdn.jtvnw.net/jtv_user_pictures/111-profile_image-70x70.jpg;msg-param-viewerCount=5;room-id=111222333;subscriber=0;system-msg=5\\sraiders\\sfrom\\sSecretRaider\\shave\\sjoined!;tmi-sent-ts=1585086853731;user-id=11223344;user-type= :tmi.twitch.tv USERNOTICE #chiswicked",
			&Message{
				Tags: &Tags{
					"badge-info":                "",
					"badges":                    "",
					"color":                     "#FF0000",
					"display-name":              "SecretRaider",
					"emotes":                    "",
					"flags":                     "",
					"id":                        "8f49ac68-7f67-9aa5-4723-4c02ab504d77",
					"login":                     "secretraider",
					"mod":                       "0",
					"msg-id":                    "raid",
					"msg-param-displayName":     "SecretRaider",
					"msg-param-login":           "secretraider",
					"msg-param-profileImageURL": "https://static-cdn.jtvnw.net/jtv_user_pictures/111-profile_image-70x70.jpg",
					"msg-param-viewerCount":     "5",
					"room-id":                   "111222333",
					"subscriber":                "0",
					"system-msg":                "5\\sraiders\\sfrom\\sSecretRaider\\shave\\sjoined!",
					"tmi-sent-ts":               "1585086853731",
					"user-id":                   "11223344",
					"user-type":                 "",
				},
				Prefix: &Prefix{
					Server: "tmi.twitch.tv",
				},
				Command:  "USERNOTICE",
				Params:   []string{"#chiswicked"},
				Trailing: "",
			},
			nil,
		},
	}

	for _, c := range cases {
		t.Run(c.in, func(t *testing.T) {
			m, err := ParseMessage(c.in)
			if !cmp.Equal(m, c.out, cmpopts.IgnoreUnexported(Message{})) {
				t.Errorf("\ngot  %v\nwant %v", m, c.out)
			}
			if err != c.err {
				t.Errorf("\ngot %v\nwant %v", err, c.err)
			}
		})
	}
}
func TestMessageRaw(t *testing.T) {
	var cases = []struct {
		in  string
		out string
	}{
		{
			"EOF",
			"EOF",
		},
		{
			"EOF\r\n",
			"EOF",
		},
		{
			// is this a valid case?
			"PING irc.atw-inter.net",
			"PING irc.atw-inter.net",
		},
		{
			"PING :irc.atw-inter.net",
			"PING :irc.atw-inter.net",
		},
		{
			"PING :irc.atw-inter.net\r\n",
			"PING :irc.atw-inter.net",
		},
		{
			":irc.atw-inter.net 001 chiswicked :Welcome to the Internet Relay Network chiswicked",
			":irc.atw-inter.net 001 chiswicked :Welcome to the Internet Relay Network chiswicked",
		},
		{
			"@aaa=bbb;ccc;example.com/ddd=eee :johndoe!johndoe@irc.atw-inter.net PRIVMSG #chiswicked :hello\r\n",
			"@aaa=bbb;ccc;example.com/ddd=eee :johndoe!johndoe@irc.atw-inter.net PRIVMSG #chiswicked :hello",
		},
		{
			":irc.atw-inter.net 005 chiswicked PENALTY FNC EXCEPTS=e INVEX=I CASEMAPPING=ascii NETWORK=IRCnet :are supported by this server",
			":irc.atw-inter.net 005 chiswicked PENALTY FNC EXCEPTS=e INVEX=I CASEMAPPING=ascii NETWORK=IRCnet :are supported by this server",
		},
	}

	for _, c := range cases {
		t.Run(c.in, func(t *testing.T) {
			m, _ := ParseMessage(c.in)
			raw := m.String()
			if raw != c.out {
				t.Errorf("\ngot  %v\nwant %v", raw, c.out)
			}
		})
	}
}

func TestMessageSender(t *testing.T) {
	var cases = []struct {
		in  string
		out string
	}{
		// Empty sender tests
		{
			"EOF\r\n",
			"",
		},
		{
			"PING :irc.atw-inter.net\r\n",
			"",
		},
		// Valid sender tests
		{
			":chiswicked!chiswicked@irc.atw-inter.net JOIN :#chiswicked\r\n",
			"chiswicked",
		},
		{
			":irc.atw-inter.net 001 chiswicked :Welcome to the Internet Relay Network chiswicked\r\n",
			"irc.atw-inter.net",
		},
		{
			":irc.atw-inter.net 005 chiswicked PENALTY FNC EXCEPTS=e INVEX=I CASEMAPPING=ascii NETWORK=IRCnet :are supported by this server\r\n",
			"irc.atw-inter.net",
		},
		{
			"@badge-info=;badges=;color=#FF0000;display-name=SecretRaider;emotes=;flags=;id=8f49ac68-7f67-9aa5-4723-4c02ab504d77;login=secretraider;mod=0;msg-id=raid;msg-param-displayName=SecretRaider;msg-param-login=secretraider;msg-param-profileImageURL=https://static-cdn.jtvnw.net/jtv_user_pictures/111-profile_image-70x70.jpg;msg-param-viewerCount=5;room-id=111222333;subscriber=0;system-msg=5\\sraiders\\sfrom\\sSecretRaider\\shave\\sjoined!;tmi-sent-ts=1585086853731;user-id=11223344;user-type= :tmi.twitch.tv USERNOTICE #chiswicked",
			"tmi.twitch.tv",
		},
	}

	for _, c := range cases {
		t.Run(c.in, func(t *testing.T) {
			m, _ := ParseMessage(c.in)
			sender := m.Sender()
			if sender != c.out {
				t.Errorf("\ngot %v\nwant %v", sender, c.out)
			}
		})
	}
}

func TestMessageReceiver(t *testing.T) {
	var cases = []struct {
		in  string
		out string
	}{
		// Empty sender tests
		{
			"EOF\r\n",
			"",
		},
		{
			"PING :irc.atw-inter.net\r\n",
			"",
		},
		{
			":chiswicked!chiswicked@irc.atw-inter.net JOIN :#chiswicked\r\n",
			"",
		},
		// Valid sender tests
		{
			":irc.atw-inter.net 001 chiswicked :Welcome to the Internet Relay Network chiswicked\r\n",
			"chiswicked",
		},
		{
			":johndoe!johndoe@irc.atw-inter.net PRIVMSG #chiswicked :hello\r\n",
			"#chiswicked",
		},
		{
			":johndoe!johndoe@irc.atw-inter.net PRIVMSG chiswicked :hello\r\n",
			"chiswicked",
		},
		{
			":irc.atw-inter.net 005 chiswicked PENALTY FNC EXCEPTS=e INVEX=I CASEMAPPING=ascii NETWORK=IRCnet :are supported by this server\r\n",
			"chiswicked",
		},
	}

	for _, c := range cases {
		t.Run(c.in, func(t *testing.T) {
			m, _ := ParseMessage(c.in)
			receiver := m.Receiver()
			if receiver != c.out {
				t.Errorf("\ngot %v\nwant %v", receiver, c.out)
			}
		})
	}
}
