// Copyright 2019 Norbert Metz
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package irc

import (
	"errors"
	"strings"
)

// Prefix represents the optional prefix of an IRC Message as per
// RFC1459 specification.
//	<servername> | <nick> [ '!' <user> ] [ '@' <host> ]
//
// See also
//
// https://tools.ietf.org/html/rfc1459#section-2.3.1
type Prefix struct {
	// Nick contains the <nick> of the sender of the message.
	// Only set if the message is a user generated message.
	Nick string
	// Server contains the <servername> of the sender of the message.
	// Only set if the message is a server generated message.
	Server string
	// User contains the <user> ident of the sender, if available.
	User string
	// Host contains the <host> of the sender, if available.
	Host string
}

// errEmptyPrefix is returned by parsePrefix if the prefix string
// passed in for parsing is an empty string. The prefix is considered empty
// if it doesn't contain anything other than whitespace.
var errEmptyPrefix = errors.New("irc: empty prefix")

// errInvalidPrefix is returned by parsePrefix if the prefix string
// passed in for parsing is invalid. A prefix is considered invalid
// if it doesn't conform to the RFC1459 specification.
var errInvalidPrefix = errors.New("irc: invalid prefix")

// parsePrefix parses the raw prefix and returns the result
// as a Prefix struct. If the raw prefix is empty or invalid,
// parsePrefix returns an ErrEmptyPrefix or ErrInvalidPrefix respectively.
// For a prefix to be considered valid, it has to conform to the
// RFC1459 specification.
func parsePrefix(prefix string) (*Prefix, error) {
	prefix = strings.TrimSpace(prefix)

	// If prefix is empty return error
	if len(prefix) == 0 {
		return nil, errEmptyPrefix
	}

	// If prefix has invalid format return error
	if strings.Index(prefix, " ") != -1 ||
		strings.Count(prefix, "@") > 1 ||
		strings.Count(prefix, "!") > 1 {
		return nil, errInvalidPrefix
	}

	i, j := strings.Index(prefix, "!"), strings.Index(prefix, "@")

	if i == 0 || i == len(prefix)-1 ||
		j == 0 || j == len(prefix)-1 ||
		i > j || j-1 == i || i-1 == j ||
		(i > -1 && j == -1) {
		return nil, errInvalidPrefix
	}

	res := &Prefix{}

	// Return Prefix <servername>
	split := strings.Split(prefix, "@")
	if len(split) == 1 {
		res.Server = prefix
		return res, nil
	}

	prefix, res.Host = split[0], split[1]

	// Return Prefix <nick>@<host>
	split = strings.Split(prefix, "!")
	if len(split) == 1 {
		res.Nick = prefix
		return res, nil
	}

	// Return Prefix <nick>!<user>@<host>
	res.Nick, res.User = split[0], split[1]

	return res, nil
}

// Sender returns the source of the message.
// If it's a server generated message it returns Server.
// If it's a user generated message it returns Nick.
func (p *Prefix) sender() string {
	if len(p.Server) > 0 {
		return p.Server
	}
	return p.Nick
}

// Copy returns a deep copy of the Prefix
func (p *Prefix) copy() *Prefix {
	return &Prefix{
		Server: p.Server,
		Nick:   p.Nick,
		User:   p.User,
		Host:   p.Host,
	}
}
