// Copyright 2019 Norbert Metz
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package irc

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestPrefixParse(t *testing.T) {
	var cases = []struct {
		in  string
		out *Prefix
		err error
	}{
		// Empty prefix tests
		{
			"",
			nil,
			errEmptyPrefix,
		},
		{
			" ",
			nil,
			errEmptyPrefix,
		},
		// Invalid prefix tests
		{
			"@",
			nil,
			errInvalidPrefix,
		},
		{
			"!",
			nil,
			errInvalidPrefix,
		},
		{
			"@!",
			nil,
			errInvalidPrefix,
		},
		{
			"!@",
			nil,
			errInvalidPrefix,
		},
		{
			"chiswicked@",
			nil,
			errInvalidPrefix,
		},
		{
			"chis wicked",
			nil,
			errInvalidPrefix,
		},
		{
			"chiswicked!",
			nil,
			errInvalidPrefix,
		},
		{
			"chiswicked@!",
			nil,
			errInvalidPrefix,
		},
		{
			"chiswicked!@",
			nil,
			errInvalidPrefix,
		},
		{
			"chiswicked!@prefix",
			nil,
			errInvalidPrefix,
		},
		{
			"@chiswicked",
			nil,
			errInvalidPrefix,
		},
		{
			"!chiswicked",
			nil,
			errInvalidPrefix,
		},
		{
			"@!chiswicked",
			nil,
			errInvalidPrefix,
		},
		{
			"!@chiswicked",
			nil,
			errInvalidPrefix,
		},
		{
			"chis@wicked@prefix",
			nil,
			errInvalidPrefix,
		},
		{
			"chiswicked!prefix",
			nil,
			errInvalidPrefix,
		},
		{
			"chis@wicked!pre!fix",
			nil,
			errInvalidPrefix,
		},
		{
			"chis!wicked@pre!fix",
			nil,
			errInvalidPrefix,
		},
		{
			"chis@wicked!pre@fix",
			nil,
			errInvalidPrefix,
		},
		// Valid prefix tests
		{
			"chiswicked",
			&Prefix{
				Server: "chiswicked",
			},
			nil,
		},
		{
			"chiswicked@chiswicked.tmi.twitch.tv",
			&Prefix{
				Nick: "chiswicked",
				Host: "chiswicked.tmi.twitch.tv",
			},
			nil,
		},
		{
			"chiswicked1!chiswicked2@chiswicked.tmi.twitch.tv",
			&Prefix{
				Nick: "chiswicked1",
				User: "chiswicked2",
				Host: "chiswicked.tmi.twitch.tv",
			},
			nil,
		},
	}

	for _, c := range cases {
		t.Run(c.in, func(t *testing.T) {
			p, err := parsePrefix(c.in)
			if !cmp.Equal(p, c.out) {
				t.Errorf("got %v, want %v", p, c.out)
			}
			if err != c.err {
				t.Errorf("got %v, want %v", err, c.err)
			}
		})
	}
}

func TestPrefixSender(t *testing.T) {
	var cases = []struct {
		in  *Prefix
		out string
	}{
		{
			in:  &Prefix{Server: "localhost"},
			out: "localhost",
		},
		{
			in:  &Prefix{Nick: "chiswicked"},
			out: "chiswicked",
		},
		{
			in:  &Prefix{Server: "localhost", Nick: "chiswicked"},
			out: "localhost",
		},
	}

	for _, c := range cases {
		t.Run("Prefix Sender()", func(t *testing.T) {
			src := c.in.sender()
			if src != c.out {
				t.Errorf("got %v, want %v", src, c.out)
			}
		})
	}
}
func TestPrefixCopy(t *testing.T) {
	p := &Prefix{Server: "server", Nick: "nick", User: "user", Host: "host"}
	pc := p.copy()
	if pc.Server != "server" || pc.Nick != "nick" || pc.User != "user" || pc.Host != "host" {
		t.Error("Prefix Copy() didn't copy contents")
	}

	pc.Server, pc.Nick, pc.User, pc.Host = "change", "the", "prefix", "contents"
	if p.Server != "server" || p.Nick != "nick" || p.User != "user" || p.Host != "host" {
		t.Error("Prefix Copy() didn't produce deep copy")
	}
}
