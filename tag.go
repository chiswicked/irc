// Copyright 2019 Norbert Metz
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package irc

import (
	"errors"
	"strings"
)

// Tags represents a structured format of IRCv3.2 message tags.
//	<tag> [';' <tag>]*
//
// See also
//
// https://ircv3.net/specs/core/message-tags-3.2.html
type Tags map[string]string

// errEmptyTags is returned by parseTags if the tags string
// passed in for parsing is an empty string. Tags are considered empty
// if it doesn't contain anything other than leading @ and/or whitespace.
var errEmptyTags = errors.New("irc: empty tags")

// errInvalidTags is returned by parseTags if the tags string
// passed in for parsing is invalid. A tag is considered invalid
// if it doesn't conform to the IRCv3.2 specification.
var errInvalidTags = errors.New("irc: invalid tags")

// parseTags parses the raw tags and returns the result
// as a Tags struct. If the raw tags is empty or invalid,
// parseTags returns an ErrEmptyTags or ErrInvalidTags respectively.
// For the tags to be considered valid, it has to conform to the
// IRCv3.2 Message Tags specification.
func parseTags(tags string) (*Tags, error) {
	tags = strings.TrimSpace(tags)

	// If tags are empty return error
	tags = strings.TrimLeft(tags, "@")
	if len(tags) == 0 {
		return nil, errEmptyTags
	}
	// If tags have invalid characters return error
	if strings.Index(tags, " ") != -1 {
		return nil, errInvalidTags
	}

	res := Tags{}

	split := strings.Split(tags, ";")
	for _, t := range split {
		// Gracefully skip empty tags (empty tag and value)
		if len(t) == 0 || t == "=" {
			continue
		}
		i := strings.Index(t, "=")
		// Standardized tag (empty value)
		if i == -1 {
			res[t] = ""
			continue
		}
		// tag=value
		res[t[:i]] = t[i+1:]
	}
	// If no valid tags found return error
	if len(res) == 0 {
		return nil, errInvalidTags
	}
	return &res, nil
}

// Copy returns a deep copy of the Tags
func (t *Tags) copy() *Tags {
	res := Tags{}
	for k, v := range *t {
		res[k] = v
	}
	return &res
}

// https://ircv3.net/specs/core/message-tags-3.2.html#escaping-values
func escapeTagVal(val string) string {
	l := len(val)
	if l == 0 {
		return ""
	}

	esc := strings.NewReplacer(
		";", "\\:",
		" ", "\\s",
		"\\", "\\\\",
		"\r", "\\r",
		"\n", "\\n",
	)
	return esc.Replace(val)
}

// https://ircv3.net/specs/core/message-tags-3.2.html#escaping-values
func unescapeTagVal(val string) string {
	l := len(val)
	if l == 0 {
		return ""
	}

	// Remove trailing lone \
	if val[l-1] == '\\' {
		if l == 1 {
			return ""
		}
		if val[l-2] != '\\' {
			val = val[:l-1]
		}
	}

	unesc := strings.NewReplacer(
		"\\:", ";",
		"\\s", " ",
		"\\\\", "\\",
		"\\r", "\r",
		"\\n", "\n",
	)
	return unesc.Replace(val)
}
