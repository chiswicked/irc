// Copyright 2019 Norbert Metz
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package irc

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestTagsParse(t *testing.T) {
	var cases = []struct {
		in  string
		out *Tags
		err error
	}{
		// Empty tags tests
		{
			"",
			nil,
			errEmptyTags,
		},
		{
			"@",
			nil,
			errEmptyTags,
		},
		{
			" ",
			nil,
			errEmptyTags,
		},
		{
			"@ ",
			nil,
			errEmptyTags,
		},
		// Invalid tags tests
		{
			"@; ",
			nil,
			errInvalidTags,
		},
		{
			"@ ; ",
			nil,
			errInvalidTags,
		},
		{
			"@;;",
			nil,
			errInvalidTags,
		},
		{
			"@some=thing; another=thing",
			nil,
			errInvalidTags,
		},
		{
			"@; ",
			nil,
			errInvalidTags,
		},
		// Valid tags tests
		{
			"@tagkey=tagvalue",
			&Tags{"tagkey": "tagvalue"},
			nil,
		},
		{
			"@tagkey=tagvalue ",
			&Tags{"tagkey": "tagvalue"},
			nil,
		},
		{
			"tagkey=tagvalue ",
			&Tags{"tagkey": "tagvalue"},
			nil,
		},
		{
			"@aaa=bbb;ccc;example.com/ddd=eee",
			&Tags{
				"aaa":             "bbb",
				"ccc":             "",
				"example.com/ddd": "eee",
			},
			nil,
		},
		{
			"@some=thing;;another=thing;;",
			&Tags{
				"some":    "thing",
				"another": "thing",
			},
			nil,
		},
		{
			"@some=thing;=;another=thing;;",
			&Tags{
				"some":    "thing",
				"another": "thing",
			},
			nil,
		},
		{
			"badges=vip/1,subscriber/6,bits-leader/1;color=;display-name=chiswicked;emotes=;flags=;id=34707dd5-5392-46f2-1234-2c12eba73987;mod=0;room-id=44351702;subscriber=1;tmi-sent-ts=1548531087701;turbo=0;user-id=913463967;user-type=",
			&Tags{
				"badges":       "vip/1,subscriber/6,bits-leader/1",
				"color":        "",
				"display-name": "chiswicked",
				"emotes":       "",
				"flags":        "",
				"id":           "34707dd5-5392-46f2-1234-2c12eba73987",
				"mod":          "0",
				"room-id":      "44351702",
				"subscriber":   "1",
				"tmi-sent-ts":  "1548531087701",
				"turbo":        "0",
				"user-id":      "913463967",
				"user-type":    "",
			},
			nil,
		},
	}

	for _, c := range cases {
		t.Run(c.in, func(t *testing.T) {
			tags, err := parseTags(c.in)
			if !cmp.Equal(tags, c.out) {
				t.Errorf("got %v, want %v", tags, c.out)
			}
			if err != c.err {
				t.Errorf("got %v, want %v", err, c.err)
			}
		})
	}
}

func TestTagsEscTagVal(t *testing.T) {
	var cases = []struct {
		in, out string
	}{
		{
			"",
			"",
		},
		{
			"test",
			"test",
		},
		{
			"te;st",
			"te\\:st",
		},
		{
			"te:st",
			"te:st",
		},
		{
			"te st",
			"te\\sst",
		},
		{
			"te\\st",
			"te\\\\st",
		},
		{
			"te\rst",
			"te\\rst",
		},
		{
			"te\nst",
			"te\\nst",
		},
		{
			"A Bit\\More Comp\\\\lex;",
			"A\\sBit\\\\More\\sComp\\\\\\\\lex\\:",
		},
		{
			"Inter\\:esting",
			"Inter\\\\:esting",
		},
		{
			"Inter\\;esting",
			"Inter\\\\\\:esting",
		},
	}

	for _, c := range cases {
		t.Run(c.in, func(t *testing.T) {
			val := escapeTagVal(c.in)
			if val != c.out {
				t.Errorf("got %v, want %v", val, c.out)
			}
		})
	}
}

func TestTagsUnescTagVal(t *testing.T) {
	var cases = []struct {
		in, out string
	}{
		{
			"",
			"",
		},
		{
			"test",
			"test",
		},
		{
			"te\\:st",
			"te;st",
		},
		{
			"te:st",
			"te:st",
		},
		{
			"te\\sst",
			"te st",
		},
		{
			"te\\\\st",
			"te\\st",
		},
		{
			"te\\rst",
			"te\rst",
		},
		{
			"te\\nst",
			"te\nst",
		},
		{
			"A\\sBit\\\\More\\sComp\\\\\\lex\\:",
			"A Bit\\More Comp\\\\lex;",
		},
		{
			"A\\sBit\\\\More\\sComp\\\\\\\\lex\\:",
			"A Bit\\More Comp\\\\lex;",
		},
		{
			"Inter\\\\:esting",
			"Inter\\:esting",
		},
		{
			"Inter\\\\\\:esting",
			"Inter\\;esting",
		},
		// Trailing lone \
		{
			"\\",
			"",
		},
		{
			"test\\",
			"test",
		},
		{
			"test\\\\",
			"test\\",
		},
	}

	for _, c := range cases {
		t.Run(c.in, func(t *testing.T) {
			val := unescapeTagVal(c.in)
			if val != c.out {
				t.Errorf("got %v, want %v", val, c.out)
			}
		})
	}
}

func TestTagsCopy(t *testing.T) {
	tags := Tags{"key1": "value1", "key2": "value2"}
	tagsc := *tags.copy()
	if tagsc["key1"] != "value1" || tagsc["key2"] != "value2" {
		t.Error("Tags Copy() didn't copy contents")
	}

	tagsc["key1"], tagsc["key2"] = "newvalue1", "newvalue2"
	if tags["key1"] != "value1" || tags["key2"] != "value2" {
		t.Error("Tags copy() didn't produce deep copy")
	}
}
